import logging
import tempfile

_logger = None


def get_logger():
    global _logger
    if _logger is not None:
        return _logger

    _logger = logging.getLogger("beedb-client")
    _logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler("%s/beedb-client.log"
                                  % tempfile.gettempdir())
    formatter = logging.Formatter("%(asctime)-23.23s | "
                                  "%(levelname)-5.5s | "
                                  "%(name)-15.15s | "
                                  "%(module)-15.15s | "
                                  "%(funcName)-12.12s | "
                                  "%(message)s")
    handler.setFormatter(formatter)
    _logger.addHandler(handler)
    requests_logger = logging.getLogger("requests.packages.urllib3")
    requests_logger.addHandler(handler)
    requests_logger.setLevel(logging.DEBUG)

    return _logger


class Log(object):
    @staticmethod
    def debug(logger, s):
        if logger is not None:
            logger.debug(s)

    @staticmethod
    def error(logger, s):
        if logger is not None:
            logger.error(s)

    @staticmethod
    def info(logger, s):
        if logger is not None:
            logger.info(s)
