===================================
BeeDB client. Pure DBaaS for clouds
===================================

-----------
Description
-----------

API client for BeeDB service

----------------------
Supported environments
----------------------

In terms of more flexible integration BeeDB client and server side does support next environments::

    OpenStack
    vCloud Air

------------
Installation
------------

In order to install BeeDB client please follow this commands::

    git clone https://github.com/ProjectInvader/beedb-client.git
    cd beedb-client
    python setup.py install

Once it was done, you will have an ability to work with BeeDB using next CLI tools::

    beedb-openstack-client
    beedb-vcloudair-client

-----
Usage
-----

To simplify CLI tool usage, BeeDB client was designed to work with OS environment variables.
Please take a look at samples directory.
In order to source all necessary parameters for authorization you need to do next::

    source openstack.rc
    source vcloud-air-ondemand.rc

or::

    source vcloud-air-subscription.rc

Once it was done, you can consume next API::

    beedb-openstack-client databases list
    beedb-openstack-client databases get --database <database>
    beedb-openstack-client databases create --database <database> \
        --description <description> \
        --database-password <passwd> \
        --database-username <username>
    beedb-openstack-client databases delete --database <database>


------------
Contribution
------------

In order to file bugs use GitHub ticketing system. Same thing for features.
